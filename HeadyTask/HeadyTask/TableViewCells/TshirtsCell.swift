//
//  TshirtsCell.swift
//  HeadyTask
//
//  Created by Apple on 22/05/20.
//  Copyright © 2020 Stanislav Ostrovskiy. All rights reserved.
//
import UIKit

class TshirtsCell: UITableViewCell {

    @IBOutlet weak var displayLable: UILabel?
    @IBOutlet weak var nameLabel: UILabel?
    
    var item: Tshirts? {
        didSet {
            guard let item = item else {
                return
            }
            displayLable?.text=item.displayName
            nameLabel?.text = item.name
			
        }
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
		sizeToFit()
		layoutIfNeeded()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        //pictureImageView?.image = nil
    }
}

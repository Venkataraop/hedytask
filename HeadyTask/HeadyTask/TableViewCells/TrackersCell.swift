//
//  TrackersCell.swift
//  HeadyTask
//
//  Created by Apple on 22/05/20.
//  Copyright © 2020 Stanislav Ostrovskiy. All rights reserved.
//
import UIKit

class TrackersCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var valueLabel: UILabel?
    
    var item: Trackers?  {
        didSet {
            titleLabel?.text = item?.name
            valueLabel?.text = item?.displayName
        }
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}

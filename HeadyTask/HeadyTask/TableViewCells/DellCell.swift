//
//  DellCell.swift
//  HeadyTask
//
//  Created by Apple on 22/05/20.
//  Copyright © 2020 Stanislav Ostrovskiy. All rights reserved.
//

import UIKit

class DellCell: UITableViewCell {

    @IBOutlet weak var displayLable: UILabel?
    @IBOutlet weak var nameLabel: UILabel?
    
    var item: Dell? {
        didSet {
            guard let item = item else {
                return
            }
            displayLable?.text=item.displayName
            nameLabel?.text = item.name
        }
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
}

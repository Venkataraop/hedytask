//
//  ProfileModel.swift
//  HeadyTask
//
//  Created by Apple on 21/05/20.
//  Copyright © 2020 Stanislav Ostrovskiy. All rights reserved.
//

import Foundation

public func dataFromFile(_ filename: String) -> Data? {
    @objc class TestClass: NSObject { }
    
    let bundle = Bundle(for: TestClass.self)
    if let path = bundle.path(forResource: filename, ofType: "json") {
        return (try? Data(contentsOf: URL(fileURLWithPath: path)))
    }
    return nil
}

class Profile {
   
    var casuals       = [Casuals]()
    var jeans         = [Jeans]()
    var tshirts       = [Tshirts]()
    var trackers      = [Trackers]()
    var formals       = [Formals]()
    var shirts        = [Shirts]()
    var apple         = [Apple]()
    var samsung       = [Samsung]()
    var dell          = [Dell]()
    var toshiba       = [Toshiba]()
    var mensware      = [Mensware]()
    var bottomware    = [Bottomware]()
    var footware      = [Footware]()
    var upperwear     = [Upperwear]()
    
    init?(data: Data) {
        
        guard let json = (try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)) as? NSArray else {
            print("Not containing JSON")
            return
        }
            let casualsValue = json [0] as? NSDictionary
            if let casuals = casualsValue?["products"] as? [[String: Any]]
            {
                self.casuals = casuals.map { Casuals(json: $0) }
            }
            let jeansValue = json [1] as? NSDictionary
            if let jeans = jeansValue?["products"] as? [[String: Any]]
            {
              self.jeans = jeans.map { Jeans(json: $0) }
            }
            let tshirtsValue = json [2] as? NSDictionary
            if let tshirts = tshirtsValue?["products"] as? [[String: Any]]
            {
               self.tshirts = tshirts.map { Tshirts(json: $0) }
            }
            let trackersValue = json [3] as? NSDictionary
            if let trackers = trackersValue?["products"] as? [[String: Any]]
            {
               self.trackers = trackers.map { Trackers(json: $0) }
            }

           let formalsValue = json [4] as? NSDictionary
           if let formals = formalsValue?["products"] as? [[String: Any]]
           {
            self.formals = formals.map { Formals(json: $0) }
           }
        
          let shirtsValue = json [5] as? NSDictionary
           if let shirts = shirtsValue?["products"] as? [[String: Any]]
           {
            self.shirts = shirts.map { Shirts(json: $0) }
           }
          let appleValue = json [6] as? NSDictionary
          if let apple = appleValue?["products"] as? [[String: Any]]
          {
            self.apple = apple.map { Apple(json: $0) }
          }
           let samsungValue = json [7] as? NSDictionary
          if let samsung = samsungValue?["products"] as? [[String: Any]]
          {
            self.samsung = samsung.map { Samsung(json: $0) }
          }
        
          let dellValue = json [8] as? NSDictionary
          if let dells = dellValue?["products"] as? [[String: Any]]
          {
            self.dell = dells.map { Dell(json: $0) }
          }
            let toshibaValue = json [9] as? NSDictionary
            if let toshibas = toshibaValue?["products"] as? [[String: Any]]
            {
            self.toshiba = toshibas.map { Toshiba(json: $0) }
           }
           let menswareValue = json [10] as? NSDictionary
           if let menswares = menswareValue?["products"] as? [[String: Any]]
          {
            self.mensware = menswares.map { Mensware(json: $0) }
          }
        
           let bottomwareValue = json [11] as? NSDictionary
           if let bottomwares = bottomwareValue?["products"] as? [[String: Any]]
           {
              self.bottomware = bottomwares.map { Bottomware(json: $0) }
           }
            let footwareValue = json [12] as? NSDictionary
           if let footwares = footwareValue?["products"] as? [[String: Any]]
           {
            self.footware = footwares.map { Footware(json: $0) }
           }
           let upperwearValue = json [13] as? NSDictionary
          if let upperwears = upperwearValue?["products"] as? [[String: Any]]
          {
            self.upperwear = upperwears.map { Upperwear(json: $0) }
          }
    }
}

class Casuals {
    var name: String?
    var displayName: String?
    
    init(json: [String: Any]) {
        self.name = json["name"] as? String
        self.displayName = json["date_added"] as? String
    }
}
class Jeans {
    var name: String?
    var displayName: String?
    init(json: [String: Any]) {
        self.name = json["name"] as? String
        self.displayName = json["date_added"] as? String
    }
}
class Tshirts {
    var name: String?
    var displayName: String?
    init(json: [String: Any]) {
        self.name = json["name"] as? String
        self.displayName = json["date_added"] as? String
    }
}

class Trackers {
    var name: String?
    var displayName: String?
    init(json: [String: Any]) {
        self.name = json["name"] as? String
        self.displayName = json["date_added"] as? String
    }
}
class Formals {
    var name: String?
    var displayName: String?
    init(json: [String: Any]) {
        self.name = json["name"] as? String
        self.displayName = json["date_added"] as? String
    }
}
class Shirts {
    var name: String?
    var displayName: String?
    init(json: [String: Any]) {
        self.name = json["name"] as? String
        self.displayName = json["date_added"] as? String
    }
}

class Apple {
    var name: String?
    var displayName: String?
    init(json: [String: Any]) {
        self.name = json["name"] as? String
        self.displayName = json["date_added"] as? String
    }
}

class Samsung {
    var name: String?
    var displayName: String?
    init(json: [String: Any]) {
        self.name = json["name"] as? String
        self.displayName = json["date_added"] as? String
    }
}

class Dell {
    var name: String?
    var displayName: String?
    init(json: [String: Any]) {
        self.name = json["name"] as? String
        self.displayName = json["date_added"] as? String
    }
}
class Toshiba {
    var name: String?
    var displayName: String?
    init(json: [String: Any]) {
        self.name = json["name"] as? String
        self.displayName = json["date_added"] as? String
    }
}
class Mensware {
    var name: String?
    var displayName: String?
    init(json: [String: Any]) {
        self.name = json["name"] as? String
        self.displayName = json["date_added"] as? String
    }
}
class Bottomware {
    var name: String?
    var displayName: String?
    init(json: [String: Any]) {
        self.name = json["name"] as? String
        self.displayName = json["date_added"] as? String
    }
}

class Footware {
    var name: String?
    var displayName: String?
    init(json: [String: Any]) {
        self.name = json["name"] as? String
        self.displayName = json["date_added"] as? String
    }
}

class Upperwear {
    var name: String?
    var displayName: String?
    init(json: [String: Any]) {
        self.name = json["name"] as? String
        self.displayName = json["date_added"] as? String
    }
}

//
//  ViewController.swift
//  HeadyTask
//
//  Created by Apple on 20/05/20.
//  Copyright © 2020 Stanislav Ostrovskiy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView?
    fileprivate let viewModel = ProfileViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.reloadSections = { [weak self] (section: Int) in
            self?.tableView?.beginUpdates()
            self?.tableView?.reloadSections([section], with: .fade)
            self?.tableView?.endUpdates()
        }
		tableView?.estimatedRowHeight = UITableView.automaticDimension
		tableView?.rowHeight = UITableView.automaticDimension
        tableView?.sectionHeaderHeight = 70
        tableView?.separatorStyle = .none
        tableView?.dataSource = viewModel
        tableView?.delegate = viewModel
        tableView?.register(CausualCell.nib, forCellReuseIdentifier: CausualCell.identifier)
        tableView?.register(JeansCell.nib, forCellReuseIdentifier: JeansCell.identifier)
        tableView?.register(TshirtsCell.nib, forCellReuseIdentifier: TshirtsCell.identifier)
        tableView?.register(TrackersCell.nib, forCellReuseIdentifier: TrackersCell.identifier)
        tableView?.register(FormalsCell.nib, forCellReuseIdentifier: FormalsCell.identifier)
        tableView?.register(ShirtsCell.nib, forCellReuseIdentifier: ShirtsCell.identifier)
        tableView?.register(AppleCell.nib, forCellReuseIdentifier: AppleCell.identifier)
        tableView?.register(SamsungCell.nib, forCellReuseIdentifier: SamsungCell.identifier)
        tableView?.register(DellCell.nib, forCellReuseIdentifier: DellCell.identifier)
        tableView?.register(ToshibaCell.nib, forCellReuseIdentifier: ToshibaCell.identifier)
        tableView?.register(MenswareCell.nib, forCellReuseIdentifier: MenswareCell.identifier)
        tableView?.register(BottomwareCell.nib, forCellReuseIdentifier: BottomwareCell.identifier)
        tableView?.register(FootwareCell.nib, forCellReuseIdentifier: FootwareCell.identifier)
        tableView?.register(UpperwearCell.nib, forCellReuseIdentifier: UpperwearCell.identifier)
        tableView?.register(HeaderView.nib, forHeaderFooterViewReuseIdentifier: HeaderView.identifier)
    }
}


//
//  ProfileViewModel.swift
//  HeadyTask
//
//  Created by Apple on 22/05/20.
//  Copyright © 2020 Stanislav Ostrovskiy. All rights reserved.
//

import Foundation
import UIKit

enum ProfileViewModelItemType {
    case casuals
    case jeans
    case tShirts
    case trackers
    case formals
    case shirts
    case apple
    case samsung
    case dell
    case toshiba
    case mensware
    case bottomware
    case footware
    case upperwear
}

protocol ProfileViewModelItem {
    var type: ProfileViewModelItemType { get }
    var sectionTitle: String { get }
    var rowCount: Int { get }
    var isCollapsible: Bool { get }
    var isCollapsed: Bool { get set }
}

extension ProfileViewModelItem {
    var rowCount: Int {
        return 1
    }
    
    var isCollapsible: Bool {
        return true
    }
}

class ProfileViewModel: NSObject {
    var items = [ProfileViewModelItem]()
    var reloadSections: ((_ section: Int) -> Void)?
    override init() {
        super.init()
        guard let data = dataFromFile("AllProducts"), let profile = Profile(data: data) else {
            return
        }
        let casuals = profile.casuals
        if !profile.casuals.isEmpty {
            let calualItem = ProfileViewModelCasualsItem(casuals: casuals)
            items.append(calualItem)
        }
        
        let jeans = profile.jeans
		if !profile.jeans.isEmpty {
            let jeansItem = ProfileViewModeljeansItem(jeans:jeans)
            items.append(jeansItem)
        }
        let tshirts = profile.tshirts
		if !profile.tshirts.isEmpty {
            let tshirtsItem = ProfileViewModelTshirtsItem(tshirts: tshirts)
            items.append(tshirtsItem)
        }
        let trackers = profile.trackers
		if !profile.trackers.isEmpty {
			let tarckersItem = ProfileViewModelTrackersItem(trackers:trackers)
            items.append(tarckersItem)
        }
        
        let formals = profile.formals
		if !profile.formals.isEmpty {
            let formalsItem = ProfileViewModelFormalsItem(formals: formals)
            items.append(formalsItem)
        }
        let shirts = profile.shirts
        if !shirts.isEmpty {
            let shirtsItem = ProfileViewModelShirtsItem(shirts: shirts)
            items.append(shirtsItem)
        }
        let apple = profile.apple
        if !apple.isEmpty {
            let appleItem = ProfileViewModelAppleItem(apple: apple)
            items.append(appleItem)
        }
        let samsung = profile.samsung
        if !samsung.isEmpty {
            let samsungItem = ProfileViewModelSamsungItem(samsung: samsung)
            items.append(samsungItem)
        }
        let dell = profile.dell
        if !dell.isEmpty {
            let dellItem = ProfileViewModelDellItem(dell: dell)
            items.append(dellItem)
        }
        let toshiba = profile.toshiba
        if !toshiba.isEmpty {
            let toshibaItem = ProfileViewModelToshibaItem(toshiba: toshiba)
            items.append(toshibaItem)
        }
        
        let mensware = profile.mensware
        if !mensware.isEmpty {
            let menswareItem = ProfileViewModelMenswareItem(mensware: mensware)
            items.append(menswareItem)
        }
        let footware = profile.footware
        if !footware.isEmpty {
            let footwareItem = ProfileViewModelFootwareItem(footware: footware)
            items.append(footwareItem)
        }
        let bottomware = profile.bottomware
        if !bottomware.isEmpty {
            let bottomwareItem = ProfileViewModelBottomwareItem(bottomware: bottomware)
            items.append(bottomwareItem)
        }
        let upperware = profile.upperwear
        if !upperware.isEmpty {
            let upperwareItem = ProfileViewModelUpperwearItem(upperwear: upperware)
            items.append(upperwareItem)
        }
    }
}

extension ProfileViewModel: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		if indexPath.section == 0
		{
			return UITableView.automaticDimension
		}
		else
		{
			return UITableView.automaticDimension
		}
    }
	func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let item = items[section]
        guard item.isCollapsible else {
            return item.rowCount
        }
        
        if item.isCollapsed {
            return 0
        } else {
            return item.rowCount
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.section]
        switch item.type {
        case .casuals:
            if let item = item as? ProfileViewModelCasualsItem, let cell = tableView.dequeueReusableCell(withIdentifier: CausualCell.identifier, for: indexPath) as? CausualCell {
                let casuals = item.casuals[indexPath.row]
                cell.item = casuals
				cell.sizeToFit()
				cell.layoutIfNeeded()
                return cell
            }
        case .jeans:
            if let item = item as? ProfileViewModeljeansItem, let cell = tableView.dequeueReusableCell(withIdentifier: JeansCell.identifier, for: indexPath) as? JeansCell {
                cell.item = item.jeans[indexPath.row]
                return cell
            }
        case .tShirts:
            if let item = item as? ProfileViewModelTshirtsItem, let cell = tableView.dequeueReusableCell(withIdentifier: TshirtsCell.identifier, for: indexPath) as? TshirtsCell {
                let tshirts = item.tshirts[indexPath.row]
                cell.item = tshirts
                return cell
            }
        case .trackers:
            if let item = item as? ProfileViewModelTrackersItem, let cell = tableView.dequeueReusableCell(withIdentifier: TrackersCell.identifier, for: indexPath) as? TrackersCell {
                let tracker = item.trackers[indexPath.row]
                cell.item = tracker
                return cell
            }
        case .formals:
            if let item = item as? ProfileViewModelFormalsItem, let cell = tableView.dequeueReusableCell(withIdentifier: FormalsCell.identifier, for: indexPath) as? FormalsCell {
                let formals = item.formals[indexPath.row]
                cell.item = formals
                return cell
            }
        case .shirts:
            if let item = item as? ProfileViewModelShirtsItem, let cell = tableView.dequeueReusableCell(withIdentifier: ShirtsCell.identifier, for: indexPath) as? ShirtsCell {
                let shirts = item.shirts[indexPath.row]
                cell.item = shirts
                return cell
            }
        case .apple:
            if let item = item as? ProfileViewModelAppleItem, let cell = tableView.dequeueReusableCell(withIdentifier: AppleCell.identifier, for: indexPath) as? AppleCell {
                let apples = item.apple[indexPath.row]
                cell.item = apples
                return cell
            }
        case .samsung:
            if let item = item as? ProfileViewModelSamsungItem, let cell = tableView.dequeueReusableCell(withIdentifier: SamsungCell.identifier, for: indexPath) as? SamsungCell {
                let samsungs = item.samsung[indexPath.row]
                cell.item = samsungs
                return cell
            }
        case .dell:
            if let item = item as? ProfileViewModelDellItem, let cell = tableView.dequeueReusableCell(withIdentifier: DellCell.identifier, for: indexPath) as? DellCell {
                let dell = item.dell[indexPath.row]
                cell.item = dell
				
                return cell
            }
        case .toshiba:
            if let item = item as? ProfileViewModelToshibaItem, let cell = tableView.dequeueReusableCell(withIdentifier: ToshibaCell.identifier, for: indexPath) as? ToshibaCell {
                let toshiba = item.toshiba[indexPath.row]
                cell.item = toshiba
                return cell
            }
        case .mensware:
            if let item = item as? ProfileViewModelMenswareItem, let cell = tableView.dequeueReusableCell(withIdentifier: MenswareCell.identifier, for: indexPath) as? MenswareCell {
                let mensware = item.mensware[indexPath.row]
                cell.item = mensware
                return cell
            }
        case .bottomware:
            if let item = item as? ProfileViewModelBottomwareItem, let cell = tableView.dequeueReusableCell(withIdentifier: BottomwareCell.identifier, for: indexPath) as? BottomwareCell {
                let bottomware = item.bottomware[indexPath.row]
                cell.item = bottomware
                return cell
            }
        case .footware:
            if let item = item as? ProfileViewModelFootwareItem, let cell = tableView.dequeueReusableCell(withIdentifier: FootwareCell.identifier, for: indexPath) as? FootwareCell {
                let footware = item.footware[indexPath.row]
                cell.item = footware
                return cell
            }
        case .upperwear:
            if let item = item as? ProfileViewModelUpperwearItem, let cell = tableView.dequeueReusableCell(withIdentifier: UpperwearCell.identifier, for: indexPath) as? UpperwearCell {
                let upperware = item.upperwear[indexPath.row]
                cell.item = upperware
                return cell
            }
        }
        return UITableViewCell()
    }
}

extension ProfileViewModel: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: HeaderView.identifier) as? HeaderView {
            let item = items[section]
            headerView.item = item
            headerView.section = section
            headerView.delegate = self
            return headerView
        }
        return UIView()
    }
}

extension ProfileViewModel: HeaderViewDelegate {
    func toggleSection(header: HeaderView, section: Int) {
        var item = items[section]
        if item.isCollapsible {
            
            // Toggle collapse
            let collapsed = !item.isCollapsed
            item.isCollapsed = collapsed
            header.setCollapsed(collapsed: collapsed)
            
            // Adjust the number of the rows inside the section
            reloadSections?(section)
        }
    }
}

class ProfileViewModelCasualsItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .casuals
    }
    var isCollapsible: Bool {
        return true
    }
    var sectionTitle: String {
        return "Casusla"
    }
    var rowCount: Int {
        return casuals.count
    }
    var isCollapsed = true
    var casuals: [Casuals]
    init(casuals: [Casuals]) {
        self.casuals = casuals
    }
}

class ProfileViewModeljeansItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .jeans
    }
    var sectionTitle: String {
        return "Jeans"
    }
    var rowCount: Int {
        return jeans.count
    }
    var isCollapsed = true
    var jeans: [Jeans]
    init(jeans: [Jeans]) {
        self.jeans = jeans
    }
}

class ProfileViewModelTshirtsItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .tShirts
    }
    var sectionTitle: String {
        return "T-Shirts"
    }
    var rowCount: Int {
        return tshirts.count
    }
    var isCollapsed = true
    var tshirts: [Tshirts]
    init(tshirts: [Tshirts]) {
        self.tshirts = tshirts
    }
}
class ProfileViewModelTrackersItem: ProfileViewModelItem {
    
    var type: ProfileViewModelItemType {
        return .trackers
    }
    var sectionTitle: String {
        return "Trackers"
    }
    var rowCount: Int {
        return trackers.count
    }
    var isCollapsed = true
    var trackers: [Trackers]
    init(trackers: [Trackers]) {
        self.trackers = trackers
    }
}
class ProfileViewModelFormalsItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .formals
    }
    var sectionTitle: String {
        return "Formals"
    }
    var rowCount: Int {
        return formals.count
    }
    var isCollapsed = true
    var formals: [Formals]
    init(formals: [Formals]) {
        self.formals = formals
    }
}
///new
class ProfileViewModelShirtsItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .shirts
    }
    var sectionTitle: String {
        return "Shirts"
    }
    var rowCount: Int {
        return shirts.count
    }
    var isCollapsed = true
    var shirts: [Shirts]
    init(shirts: [Shirts]) {
        self.shirts = shirts
    }
}
class ProfileViewModelAppleItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .apple
    }
    var sectionTitle: String {
        return "Apple"
    }
    var rowCount: Int {
        return apple.count
    }
    var isCollapsed = true
    var apple: [Apple]
    init(apple: [Apple]) {
        self.apple = apple
    }
}
class ProfileViewModelSamsungItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .samsung
    }
    var sectionTitle: String {
        return "Samsung"
    }
    var rowCount: Int {
        return samsung.count
    }
    var isCollapsed = true
    var samsung: [Samsung]
    init(samsung: [Samsung]) {
        self.samsung = samsung
    }
}
class ProfileViewModelDellItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .dell
    }
    var sectionTitle: String {
        return "Dell"
    }
    var rowCount: Int {
        return dell.count
    }
    var isCollapsed = true
    var dell: [Dell]
    init(dell: [Dell]) {
        self.dell = dell
    }
}
class ProfileViewModelToshibaItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .toshiba
    }
    var sectionTitle: String {
        return "Toshiba"
    }
    var rowCount: Int {
        return toshiba.count
    }
    var isCollapsed = true
    var toshiba: [Toshiba]
    init(toshiba: [Toshiba]) {
        self.toshiba = toshiba
    }
}
class ProfileViewModelMenswareItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .mensware
    }
    var sectionTitle: String {
        return "Mensware"
    }
    var rowCount: Int {
        return mensware.count
    }
    var isCollapsed = true
    var mensware: [Mensware]
    init(mensware: [Mensware]) {
        self.mensware = mensware
    }
}
class ProfileViewModelBottomwareItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .bottomware
    }
    var sectionTitle: String {
        return "Bottomware"
    }
    var rowCount: Int {
        return bottomware.count
    }
    var isCollapsed = true
    var bottomware: [Bottomware]
    init(bottomware: [Bottomware]) {
        self.bottomware = bottomware
    }
}
class ProfileViewModelFootwareItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .footware
    }
    var sectionTitle: String {
        return "Footware"
    }
    var rowCount: Int {
        return footware.count
    }
    var isCollapsed = true
    var footware: [Footware]
    init(footware: [Footware]) {
        self.footware = footware
    }
}
class ProfileViewModelUpperwearItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .upperwear
    }
    var sectionTitle: String {
        return "Upperwear"
    }
    var rowCount: Int {
        return upperwear.count
    }
    var isCollapsed = true
    var upperwear: [Upperwear]
    init(upperwear: [Upperwear]) {
        self.upperwear = upperwear
    }
}

